package org.gitnex.tea4j.v2.apis.custom;

import java.util.List;
import org.gitnex.tea4j.v2.models.*;
import retrofit2.Call;
import retrofit2.http.*;

public interface CustomApi {

  @GET("repos/{owner}/{repo}/contents/{filepath}")
  Call<List<ContentsResponse>> repoGetContentsList(
      @Path("owner") String owner,
      @Path("repo") String repo,
      @Path(value = "filepath", encoded = false) String filepath,
      @Query("ref") String ref);

  /**
   * Remove a reaction from a comment of an issue
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id id of the comment to edit (required)
   * @param body (optional)
   * @return Call<Void>;
   */
  @Headers({"Content-Type:application/json"})
  @HTTP(
      method = "DELETE",
      path = "repos/{owner}/{repo}/issues/comments/{id}/reactions",
      hasBody = true)
  Call<Void> issueDeleteCommentReactionWithBody(
      @Path("owner") String owner,
      @Path("repo") String repo,
      @Path("id") Long id,
      @Body EditReactionOption body);

  /**
   * Remove a reaction from an issue
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the issue (required)
   * @param body (optional)
   * @return Call<Void>;
   */
  @Headers({"Content-Type:application/json"})
  @HTTP(method = "DELETE", path = "repos/{owner}/{repo}/issues/{index}/reactions", hasBody = true)
  Call<Void> issueDeleteIssueReactionWithBody(
      @Path("owner") String owner,
      @Path("repo") String repo,
      @Path("index") Long index,
      @Body EditReactionOption body);

  /**
   * Delete a file in a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param filepath path of the file to delete (required)
   * @param body (required)
   * @return Call<FileDeleteResponse>;
   */
  @Headers({"Content-Type:application/json"})
  @HTTP(method = "DELETE", path = "repos/{owner}/{repo}/contents/{filepath}", hasBody = true)
  Call<FileDeleteResponse> repoDeleteFileWithBody(
      @Path("owner") String owner,
      @Path("repo") String repo,
      @Path("filepath") String filepath,
      @Body DeleteFileOptions body);

  /**
   * Cancel review requests for a pull request
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the pull request (required)
   * @param body (required)
   * @return Call<Void>;
   */
  @Headers({"Content-Type:application/json"})
  @HTTP(
      method = "DELETE",
      path = "repos/{owner}/{repo}/pulls/{index}/requested_reviewers",
      hasBody = true)
  Call<Void> repoDeletePullReviewRequestsWithBody(
      @Path("owner") String owner,
      @Path("repo") String repo,
      @Path("index") Long index,
      @Body PullReviewRequestOptions body);

  /**
   * Delete email addresses
   *
   * @param body (optional)
   * @return Call<Void>;
   */
  @Headers({"Content-Type:application/json"})
  @HTTP(method = "DELETE", path = "user/emails", hasBody = true)
  Call<Void> userDeleteEmailWithBody(@Body DeleteEmailOption body);

  @Headers({"Content-Type:application/json"})
  @GET("notifications")
  Call<List<NotificationThread>> notifyGetList2(
      @retrofit2.http.Query("all") Boolean all,
      @retrofit2.http.Query("status-types") List<String> statusTypes,
      @retrofit2.http.Query("subject-type") List<String> subjectType,
      @retrofit2.http.Query("since") String since,
      @retrofit2.http.Query("before") String before,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  @GET("user/settings")
    Call<UserSettings> customGetUserSettings();

    @Headers({"Content-Type:application/json"})
    @PATCH("user/settings")
    Call<UserSettings> customUpdateUserSettings(@Body UserSettingsOptions userSettingsOptions);
}
