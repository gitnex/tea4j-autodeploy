/*
 * Gitea API
 * This documentation describes the Gitea API.
 *
 * OpenAPI spec version: {{AppVer | JSEscape}}
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package org.gitnex.tea4j.v2.models;

import com.google.gson.annotations.SerializedName;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.Objects;

/** LicensesListEntry is used for the API */
@Schema(description = "LicensesListEntry is used for the API")
public class LicensesTemplateListEntry implements Serializable {
  private static final long serialVersionUID = 1L;

  @SerializedName("key")
  private String key = null;

  @SerializedName("name")
  private String name = null;

  @SerializedName("url")
  private String url = null;

  public LicensesTemplateListEntry key(String key) {
    this.key = key;
    return this;
  }

  /**
   * Get key
   *
   * @return key
   */
  @Schema(description = "")
  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public LicensesTemplateListEntry name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   *
   * @return name
   */
  @Schema(description = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public LicensesTemplateListEntry url(String url) {
    this.url = url;
    return this;
  }

  /**
   * Get url
   *
   * @return url
   */
  @Schema(description = "")
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LicensesTemplateListEntry licensesTemplateListEntry = (LicensesTemplateListEntry) o;
    return Objects.equals(this.key, licensesTemplateListEntry.key)
        && Objects.equals(this.name, licensesTemplateListEntry.name)
        && Objects.equals(this.url, licensesTemplateListEntry.url);
  }

  @Override
  public int hashCode() {
    return Objects.hash(key, name, url);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LicensesTemplateListEntry {\n");

    sb.append("    key: ").append(toIndentedString(key)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
