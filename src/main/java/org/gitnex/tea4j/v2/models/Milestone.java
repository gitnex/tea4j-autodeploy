/*
 * Gitea API
 * This documentation describes the Gitea API.
 *
 * OpenAPI spec version: {{AppVer | JSEscape}}
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package org.gitnex.tea4j.v2.models;

import com.google.gson.annotations.SerializedName;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/** Milestone milestone is a collection of issues on one repository */
@Schema(description = "Milestone milestone is a collection of issues on one repository")
public class Milestone implements Serializable {
  private static final long serialVersionUID = 1L;

  @SerializedName("closed_at")
  private Date closedAt = null;

  @SerializedName("closed_issues")
  private Long closedIssues = null;

  @SerializedName("created_at")
  private Date createdAt = null;

  @SerializedName("description")
  private String description = null;

  @SerializedName("due_on")
  private Date dueOn = null;

  @SerializedName("id")
  private Long id = null;

  @SerializedName("open_issues")
  private Long openIssues = null;

  @SerializedName("state")
  private String state = null;

  @SerializedName("title")
  private String title = null;

  @SerializedName("updated_at")
  private Date updatedAt = null;

  public Milestone closedAt(Date closedAt) {
    this.closedAt = closedAt;
    return this;
  }

  /**
   * Get closedAt
   *
   * @return closedAt
   */
  @Schema(description = "")
  public Date getClosedAt() {
    return closedAt;
  }

  public void setClosedAt(Date closedAt) {
    this.closedAt = closedAt;
  }

  public Milestone closedIssues(Long closedIssues) {
    this.closedIssues = closedIssues;
    return this;
  }

  /**
   * Get closedIssues
   *
   * @return closedIssues
   */
  @Schema(description = "")
  public Long getClosedIssues() {
    return closedIssues;
  }

  public void setClosedIssues(Long closedIssues) {
    this.closedIssues = closedIssues;
  }

  public Milestone createdAt(Date createdAt) {
    this.createdAt = createdAt;
    return this;
  }

  /**
   * Get createdAt
   *
   * @return createdAt
   */
  @Schema(description = "")
  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Milestone description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Get description
   *
   * @return description
   */
  @Schema(description = "")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Milestone dueOn(Date dueOn) {
    this.dueOn = dueOn;
    return this;
  }

  /**
   * Get dueOn
   *
   * @return dueOn
   */
  @Schema(description = "")
  public Date getDueOn() {
    return dueOn;
  }

  public void setDueOn(Date dueOn) {
    this.dueOn = dueOn;
  }

  public Milestone id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   *
   * @return id
   */
  @Schema(description = "")
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Milestone openIssues(Long openIssues) {
    this.openIssues = openIssues;
    return this;
  }

  /**
   * Get openIssues
   *
   * @return openIssues
   */
  @Schema(description = "")
  public Long getOpenIssues() {
    return openIssues;
  }

  public void setOpenIssues(Long openIssues) {
    this.openIssues = openIssues;
  }

  public Milestone state(String state) {
    this.state = state;
    return this;
  }

  /**
   * Get state
   *
   * @return state
   */
  @Schema(description = "")
  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public Milestone title(String title) {
    this.title = title;
    return this;
  }

  /**
   * Get title
   *
   * @return title
   */
  @Schema(description = "")
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Milestone updatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

  /**
   * Get updatedAt
   *
   * @return updatedAt
   */
  @Schema(description = "")
  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Milestone milestone = (Milestone) o;
    return Objects.equals(this.closedAt, milestone.closedAt)
        && Objects.equals(this.closedIssues, milestone.closedIssues)
        && Objects.equals(this.createdAt, milestone.createdAt)
        && Objects.equals(this.description, milestone.description)
        && Objects.equals(this.dueOn, milestone.dueOn)
        && Objects.equals(this.id, milestone.id)
        && Objects.equals(this.openIssues, milestone.openIssues)
        && Objects.equals(this.state, milestone.state)
        && Objects.equals(this.title, milestone.title)
        && Objects.equals(this.updatedAt, milestone.updatedAt);
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        closedAt,
        closedIssues,
        createdAt,
        description,
        dueOn,
        id,
        openIssues,
        state,
        title,
        updatedAt);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Milestone {\n");

    sb.append("    closedAt: ").append(toIndentedString(closedAt)).append("\n");
    sb.append("    closedIssues: ").append(toIndentedString(closedIssues)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    dueOn: ").append(toIndentedString(dueOn)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    openIssues: ").append(toIndentedString(openIssues)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
