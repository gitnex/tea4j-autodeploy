/*
 * Gitea API
 * This documentation describes the Gitea API.
 *
 * OpenAPI spec version: {{AppVer | JSEscape}}
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package org.gitnex.tea4j.v2.models;

import com.google.gson.annotations.SerializedName;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/** CreatePullRequestOption options when creating a pull request */
@Schema(description = "CreatePullRequestOption options when creating a pull request")
public class CreatePullRequestOption implements Serializable {
  private static final long serialVersionUID = 1L;

  @SerializedName("assignee")
  private String assignee = null;

  @SerializedName("assignees")
  private List<String> assignees = null;

  @SerializedName("base")
  private String base = null;

  @SerializedName("body")
  private String body = null;

  @SerializedName("due_date")
  private Date dueDate = null;

  @SerializedName("head")
  private String head = null;

  @SerializedName("labels")
  private List<Long> labels = null;

  @SerializedName("milestone")
  private Long milestone = null;

  @SerializedName("reviewers")
  private List<String> reviewers = null;

  @SerializedName("team_reviewers")
  private List<String> teamReviewers = null;

  @SerializedName("title")
  private String title = null;

  public CreatePullRequestOption assignee(String assignee) {
    this.assignee = assignee;
    return this;
  }

  /**
   * Get assignee
   *
   * @return assignee
   */
  @Schema(description = "")
  public String getAssignee() {
    return assignee;
  }

  public void setAssignee(String assignee) {
    this.assignee = assignee;
  }

  public CreatePullRequestOption assignees(List<String> assignees) {
    this.assignees = assignees;
    return this;
  }

  public CreatePullRequestOption addAssigneesItem(String assigneesItem) {
    if (this.assignees == null) {
      this.assignees = new ArrayList<>();
    }
    this.assignees.add(assigneesItem);
    return this;
  }

  /**
   * Get assignees
   *
   * @return assignees
   */
  @Schema(description = "")
  public List<String> getAssignees() {
    return assignees;
  }

  public void setAssignees(List<String> assignees) {
    this.assignees = assignees;
  }

  public CreatePullRequestOption base(String base) {
    this.base = base;
    return this;
  }

  /**
   * Get base
   *
   * @return base
   */
  @Schema(description = "")
  public String getBase() {
    return base;
  }

  public void setBase(String base) {
    this.base = base;
  }

  public CreatePullRequestOption body(String body) {
    this.body = body;
    return this;
  }

  /**
   * Get body
   *
   * @return body
   */
  @Schema(description = "")
  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public CreatePullRequestOption dueDate(Date dueDate) {
    this.dueDate = dueDate;
    return this;
  }

  /**
   * Get dueDate
   *
   * @return dueDate
   */
  @Schema(description = "")
  public Date getDueDate() {
    return dueDate;
  }

  public void setDueDate(Date dueDate) {
    this.dueDate = dueDate;
  }

  public CreatePullRequestOption head(String head) {
    this.head = head;
    return this;
  }

  /**
   * Get head
   *
   * @return head
   */
  @Schema(description = "")
  public String getHead() {
    return head;
  }

  public void setHead(String head) {
    this.head = head;
  }

  public CreatePullRequestOption labels(List<Long> labels) {
    this.labels = labels;
    return this;
  }

  public CreatePullRequestOption addLabelsItem(Long labelsItem) {
    if (this.labels == null) {
      this.labels = new ArrayList<>();
    }
    this.labels.add(labelsItem);
    return this;
  }

  /**
   * Get labels
   *
   * @return labels
   */
  @Schema(description = "")
  public List<Long> getLabels() {
    return labels;
  }

  public void setLabels(List<Long> labels) {
    this.labels = labels;
  }

  public CreatePullRequestOption milestone(Long milestone) {
    this.milestone = milestone;
    return this;
  }

  /**
   * Get milestone
   *
   * @return milestone
   */
  @Schema(description = "")
  public Long getMilestone() {
    return milestone;
  }

  public void setMilestone(Long milestone) {
    this.milestone = milestone;
  }

  public CreatePullRequestOption reviewers(List<String> reviewers) {
    this.reviewers = reviewers;
    return this;
  }

  public CreatePullRequestOption addReviewersItem(String reviewersItem) {
    if (this.reviewers == null) {
      this.reviewers = new ArrayList<>();
    }
    this.reviewers.add(reviewersItem);
    return this;
  }

  /**
   * Get reviewers
   *
   * @return reviewers
   */
  @Schema(description = "")
  public List<String> getReviewers() {
    return reviewers;
  }

  public void setReviewers(List<String> reviewers) {
    this.reviewers = reviewers;
  }

  public CreatePullRequestOption teamReviewers(List<String> teamReviewers) {
    this.teamReviewers = teamReviewers;
    return this;
  }

  public CreatePullRequestOption addTeamReviewersItem(String teamReviewersItem) {
    if (this.teamReviewers == null) {
      this.teamReviewers = new ArrayList<>();
    }
    this.teamReviewers.add(teamReviewersItem);
    return this;
  }

  /**
   * Get teamReviewers
   *
   * @return teamReviewers
   */
  @Schema(description = "")
  public List<String> getTeamReviewers() {
    return teamReviewers;
  }

  public void setTeamReviewers(List<String> teamReviewers) {
    this.teamReviewers = teamReviewers;
  }

  public CreatePullRequestOption title(String title) {
    this.title = title;
    return this;
  }

  /**
   * Get title
   *
   * @return title
   */
  @Schema(description = "")
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CreatePullRequestOption createPullRequestOption = (CreatePullRequestOption) o;
    return Objects.equals(this.assignee, createPullRequestOption.assignee)
        && Objects.equals(this.assignees, createPullRequestOption.assignees)
        && Objects.equals(this.base, createPullRequestOption.base)
        && Objects.equals(this.body, createPullRequestOption.body)
        && Objects.equals(this.dueDate, createPullRequestOption.dueDate)
        && Objects.equals(this.head, createPullRequestOption.head)
        && Objects.equals(this.labels, createPullRequestOption.labels)
        && Objects.equals(this.milestone, createPullRequestOption.milestone)
        && Objects.equals(this.reviewers, createPullRequestOption.reviewers)
        && Objects.equals(this.teamReviewers, createPullRequestOption.teamReviewers)
        && Objects.equals(this.title, createPullRequestOption.title);
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        assignee,
        assignees,
        base,
        body,
        dueDate,
        head,
        labels,
        milestone,
        reviewers,
        teamReviewers,
        title);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CreatePullRequestOption {\n");

    sb.append("    assignee: ").append(toIndentedString(assignee)).append("\n");
    sb.append("    assignees: ").append(toIndentedString(assignees)).append("\n");
    sb.append("    base: ").append(toIndentedString(base)).append("\n");
    sb.append("    body: ").append(toIndentedString(body)).append("\n");
    sb.append("    dueDate: ").append(toIndentedString(dueDate)).append("\n");
    sb.append("    head: ").append(toIndentedString(head)).append("\n");
    sb.append("    labels: ").append(toIndentedString(labels)).append("\n");
    sb.append("    milestone: ").append(toIndentedString(milestone)).append("\n");
    sb.append("    reviewers: ").append(toIndentedString(reviewers)).append("\n");
    sb.append("    teamReviewers: ").append(toIndentedString(teamReviewers)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
