/*
 * Gitea API
 * This documentation describes the Gitea API.
 *
 * OpenAPI spec version: {{AppVer | JSEscape}}
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package org.gitnex.tea4j.v2.models;

import com.google.gson.annotations.SerializedName;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/** DeleteEmailOption options when deleting email addresses */
@Schema(description = "DeleteEmailOption options when deleting email addresses")
public class DeleteEmailOption implements Serializable {
  private static final long serialVersionUID = 1L;

  @SerializedName("emails")
  private List<String> emails = null;

  public DeleteEmailOption emails(List<String> emails) {
    this.emails = emails;
    return this;
  }

  public DeleteEmailOption addEmailsItem(String emailsItem) {
    if (this.emails == null) {
      this.emails = new ArrayList<>();
    }
    this.emails.add(emailsItem);
    return this;
  }

  /**
   * email addresses to delete
   *
   * @return emails
   */
  @Schema(description = "email addresses to delete")
  public List<String> getEmails() {
    return emails;
  }

  public void setEmails(List<String> emails) {
    this.emails = emails;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeleteEmailOption deleteEmailOption = (DeleteEmailOption) o;
    return Objects.equals(this.emails, deleteEmailOption.emails);
  }

  @Override
  public int hashCode() {
    return Objects.hash(emails);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DeleteEmailOption {\n");

    sb.append("    emails: ").append(toIndentedString(emails)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
