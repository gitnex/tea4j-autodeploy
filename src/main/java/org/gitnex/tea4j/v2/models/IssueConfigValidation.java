/*
 * Gitea API
 * This documentation describes the Gitea API.
 *
 * OpenAPI spec version: {{AppVer | JSEscape}}
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package org.gitnex.tea4j.v2.models;

import com.google.gson.annotations.SerializedName;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.Objects;

/** IssueConfigValidation */
public class IssueConfigValidation implements Serializable {
  private static final long serialVersionUID = 1L;

  @SerializedName("message")
  private String message = null;

  @SerializedName("valid")
  private Boolean valid = null;

  public IssueConfigValidation message(String message) {
    this.message = message;
    return this;
  }

  /**
   * Get message
   *
   * @return message
   */
  @Schema(description = "")
  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public IssueConfigValidation valid(Boolean valid) {
    this.valid = valid;
    return this;
  }

  /**
   * Get valid
   *
   * @return valid
   */
  @Schema(description = "")
  public Boolean isValid() {
    return valid;
  }

  public void setValid(Boolean valid) {
    this.valid = valid;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    IssueConfigValidation issueConfigValidation = (IssueConfigValidation) o;
    return Objects.equals(this.message, issueConfigValidation.message)
        && Objects.equals(this.valid, issueConfigValidation.valid);
  }

  @Override
  public int hashCode() {
    return Objects.hash(message, valid);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class IssueConfigValidation {\n");

    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("    valid: ").append(toIndentedString(valid)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
