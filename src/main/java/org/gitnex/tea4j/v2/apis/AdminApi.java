package org.gitnex.tea4j.v2.apis;

import java.util.List;
import org.gitnex.tea4j.v2.CollectionFormats.*;
import org.gitnex.tea4j.v2.models.Badge;
import org.gitnex.tea4j.v2.models.CreateHookOption;
import org.gitnex.tea4j.v2.models.CreateKeyOption;
import org.gitnex.tea4j.v2.models.CreateOrgOption;
import org.gitnex.tea4j.v2.models.CreateRepoOption;
import org.gitnex.tea4j.v2.models.CreateUserOption;
import org.gitnex.tea4j.v2.models.Cron;
import org.gitnex.tea4j.v2.models.EditHookOption;
import org.gitnex.tea4j.v2.models.EditUserOption;
import org.gitnex.tea4j.v2.models.Email;
import org.gitnex.tea4j.v2.models.Hook;
import org.gitnex.tea4j.v2.models.Organization;
import org.gitnex.tea4j.v2.models.PublicKey;
import org.gitnex.tea4j.v2.models.RenameUserOption;
import org.gitnex.tea4j.v2.models.Repository;
import org.gitnex.tea4j.v2.models.User;
import org.gitnex.tea4j.v2.models.UserBadgeOption;
import retrofit2.Call;
import retrofit2.http.*;

public interface AdminApi {
  /**
   * Add a badge to a user
   *
   * @param username username of user (required)
   * @param body (optional)
   * @return Call&lt;Void&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("admin/users/{username}/badges")
  Call<Void> adminAddUserBadges(
      @retrofit2.http.Path("username") String username, @retrofit2.http.Body UserBadgeOption body);

  /**
   * Adopt unadopted files as a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;Void&gt;
   */
  @POST("admin/unadopted/{owner}/{repo}")
  Call<Void> adminAdoptRepository(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Create a hook
   *
   * @param body (required)
   * @return Call&lt;Hook&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("admin/hooks")
  Call<Hook> adminCreateHook(@retrofit2.http.Body CreateHookOption body);

  /**
   * Create an organization
   *
   * @param body (required)
   * @param username username of the user that will own the created organization (required)
   * @return Call&lt;Organization&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("admin/users/{username}/orgs")
  Call<Organization> adminCreateOrg(
      @retrofit2.http.Body CreateOrgOption body, @retrofit2.http.Path("username") String username);

  /**
   * Add a public key on behalf of a user
   *
   * @param username username of the user (required)
   * @param body (optional)
   * @return Call&lt;PublicKey&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("admin/users/{username}/keys")
  Call<PublicKey> adminCreatePublicKey(
      @retrofit2.http.Path("username") String username, @retrofit2.http.Body CreateKeyOption body);

  /**
   * Create a repository on behalf of a user
   *
   * @param body (required)
   * @param username username of the user. This user will own the created repository (required)
   * @return Call&lt;Repository&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("admin/users/{username}/repos")
  Call<Repository> adminCreateRepo(
      @retrofit2.http.Body CreateRepoOption body, @retrofit2.http.Path("username") String username);

  /**
   * Create a user
   *
   * @param body (optional)
   * @return Call&lt;User&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("admin/users")
  Call<User> adminCreateUser(@retrofit2.http.Body CreateUserOption body);

  /**
   * List cron tasks
   *
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;Cron&gt;&gt;
   */
  @GET("admin/cron")
  Call<List<Cron>> adminCronList(
      @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("limit") Integer limit);

  /**
   * Run cron task
   *
   * @param task task to run (required)
   * @return Call&lt;Void&gt;
   */
  @POST("admin/cron/{task}")
  Call<Void> adminCronRun(@retrofit2.http.Path("task") String task);

  /**
   * Delete a hook
   *
   * @param id id of the hook to delete (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("admin/hooks/{id}")
  Call<Void> adminDeleteHook(@retrofit2.http.Path("id") Long id);

  /**
   * Delete unadopted files
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("admin/unadopted/{owner}/{repo}")
  Call<Void> adminDeleteUnadoptedRepository(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Delete a user
   *
   * @param username username of user to delete (required)
   * @param purge purge the user from the system completely (optional)
   * @return Call&lt;Void&gt;
   */
  @DELETE("admin/users/{username}")
  Call<Void> adminDeleteUser(
      @retrofit2.http.Path("username") String username,
      @retrofit2.http.Query("purge") Boolean purge);

  /**
   * Remove a badge from a user
   *
   * @param username username of user (required)
   * @param body (optional)
   * @return Call&lt;Void&gt;
   */
  @Headers({"Content-Type:application/json"})
  @DELETE("admin/users/{username}/badges")
  Call<Void> adminDeleteUserBadges(
      @retrofit2.http.Path("username") String username, @retrofit2.http.Body UserBadgeOption body);

  /**
   * Delete a user&#x27;s public key
   *
   * @param username username of user (required)
   * @param id id of the key to delete (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("admin/users/{username}/keys/{id}")
  Call<Void> adminDeleteUserPublicKey(
      @retrofit2.http.Path("username") String username, @retrofit2.http.Path("id") Long id);

  /**
   * Update a hook
   *
   * @param id id of the hook to update (required)
   * @param body (optional)
   * @return Call&lt;Hook&gt;
   */
  @Headers({"Content-Type:application/json"})
  @PATCH("admin/hooks/{id}")
  Call<Hook> adminEditHook(
      @retrofit2.http.Path("id") Long id, @retrofit2.http.Body EditHookOption body);

  /**
   * Edit an existing user
   *
   * @param username username of user to edit (required)
   * @param body (optional)
   * @return Call&lt;User&gt;
   */
  @Headers({"Content-Type:application/json"})
  @PATCH("admin/users/{username}")
  Call<User> adminEditUser(
      @retrofit2.http.Path("username") String username, @retrofit2.http.Body EditUserOption body);

  /**
   * List all emails
   *
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;Email&gt;&gt;
   */
  @GET("admin/emails")
  Call<List<Email>> adminGetAllEmails(
      @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("limit") Integer limit);

  /**
   * List all organizations
   *
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;Organization&gt;&gt;
   */
  @GET("admin/orgs")
  Call<List<Organization>> adminGetAllOrgs(
      @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("limit") Integer limit);

  /**
   * Get a hook
   *
   * @param id id of the hook to get (required)
   * @return Call&lt;Hook&gt;
   */
  @GET("admin/hooks/{id}")
  Call<Hook> adminGetHook(@retrofit2.http.Path("id") Long id);

  /**
   * Get an global actions runner registration token
   *
   * @return Call&lt;Void&gt;
   */
  @GET("admin/runners/registration-token")
  Call<Void> adminGetRunnerRegistrationToken();

  /**
   * List system&#x27;s webhooks
   *
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @param type system, default or both kinds of webhooks (optional, default to system)
   * @return Call&lt;List&lt;Hook&gt;&gt;
   */
  @GET("admin/hooks")
  Call<List<Hook>> adminListHooks(
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit,
      @retrofit2.http.Query("type") String type);

  /**
   * List a user&#x27;s badges
   *
   * @param username username of user (required)
   * @return Call&lt;List&lt;Badge&gt;&gt;
   */
  @GET("admin/users/{username}/badges")
  Call<List<Badge>> adminListUserBadges(@retrofit2.http.Path("username") String username);

  /**
   * Rename a user
   *
   * @param body (required)
   * @param username existing username of user (required)
   * @return Call&lt;Void&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("admin/users/{username}/rename")
  Call<Void> adminRenameUser(
      @retrofit2.http.Body RenameUserOption body, @retrofit2.http.Path("username") String username);

  /**
   * Search all emails
   *
   * @param q keyword (optional)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;Email&gt;&gt;
   */
  @GET("admin/emails/search")
  Call<List<Email>> adminSearchEmails(
      @retrofit2.http.Query("q") String q,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * Search users according filter conditions
   *
   * @param sourceId ID of the user&#x27;s login source to search for (optional)
   * @param loginName user&#x27;s login name to search for (optional)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;User&gt;&gt;
   */
  @GET("admin/users")
  Call<List<User>> adminSearchUsers(
      @retrofit2.http.Query("source_id") Long sourceId,
      @retrofit2.http.Query("login_name") String loginName,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List unadopted repositories
   *
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @param pattern pattern of repositories to search for (optional)
   * @return Call&lt;List&lt;String&gt;&gt;
   */
  @GET("admin/unadopted")
  Call<List<String>> adminUnadoptedList(
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit,
      @retrofit2.http.Query("pattern") String pattern);
}
