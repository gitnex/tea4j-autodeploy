package org.gitnex.tea4j.v2.apis;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;
import okhttp3.RequestBody;
import org.gitnex.tea4j.v2.CollectionFormats.*;
import org.gitnex.tea4j.v2.models.ActionTaskResponse;
import org.gitnex.tea4j.v2.models.ActionVariable;
import org.gitnex.tea4j.v2.models.Activity;
import org.gitnex.tea4j.v2.models.AddCollaboratorOption;
import org.gitnex.tea4j.v2.models.AnnotatedTag;
import org.gitnex.tea4j.v2.models.Attachment;
import org.gitnex.tea4j.v2.models.Branch;
import org.gitnex.tea4j.v2.models.BranchProtection;
import org.gitnex.tea4j.v2.models.ChangeFilesOptions;
import org.gitnex.tea4j.v2.models.ChangedFile;
import org.gitnex.tea4j.v2.models.CombinedStatus;
import org.gitnex.tea4j.v2.models.Commit;
import org.gitnex.tea4j.v2.models.CommitStatus;
import org.gitnex.tea4j.v2.models.Compare;
import org.gitnex.tea4j.v2.models.ContentsResponse;
import org.gitnex.tea4j.v2.models.CreateBranchProtectionOption;
import org.gitnex.tea4j.v2.models.CreateBranchRepoOption;
import org.gitnex.tea4j.v2.models.CreateFileOptions;
import org.gitnex.tea4j.v2.models.CreateForkOption;
import org.gitnex.tea4j.v2.models.CreateHookOption;
import org.gitnex.tea4j.v2.models.CreateKeyOption;
import org.gitnex.tea4j.v2.models.CreateOrUpdateSecretOption;
import org.gitnex.tea4j.v2.models.CreatePullRequestOption;
import org.gitnex.tea4j.v2.models.CreatePullReviewOptions;
import org.gitnex.tea4j.v2.models.CreatePushMirrorOption;
import org.gitnex.tea4j.v2.models.CreateReleaseOption;
import org.gitnex.tea4j.v2.models.CreateRepoOption;
import org.gitnex.tea4j.v2.models.CreateStatusOption;
import org.gitnex.tea4j.v2.models.CreateTagOption;
import org.gitnex.tea4j.v2.models.CreateTagProtectionOption;
import org.gitnex.tea4j.v2.models.CreateVariableOption;
import org.gitnex.tea4j.v2.models.CreateWikiPageOptions;
import org.gitnex.tea4j.v2.models.DeleteFileOptions;
import org.gitnex.tea4j.v2.models.DeployKey;
import org.gitnex.tea4j.v2.models.DismissPullReviewOptions;
import org.gitnex.tea4j.v2.models.EditAttachmentOptions;
import org.gitnex.tea4j.v2.models.EditBranchProtectionOption;
import org.gitnex.tea4j.v2.models.EditGitHookOption;
import org.gitnex.tea4j.v2.models.EditHookOption;
import org.gitnex.tea4j.v2.models.EditPullRequestOption;
import org.gitnex.tea4j.v2.models.EditReleaseOption;
import org.gitnex.tea4j.v2.models.EditRepoOption;
import org.gitnex.tea4j.v2.models.EditTagProtectionOption;
import org.gitnex.tea4j.v2.models.FileDeleteResponse;
import org.gitnex.tea4j.v2.models.FileResponse;
import org.gitnex.tea4j.v2.models.FilesResponse;
import org.gitnex.tea4j.v2.models.GenerateRepoOption;
import org.gitnex.tea4j.v2.models.GitBlobResponse;
import org.gitnex.tea4j.v2.models.GitHook;
import org.gitnex.tea4j.v2.models.GitTreeResponse;
import org.gitnex.tea4j.v2.models.Hook;
import org.gitnex.tea4j.v2.models.IdAssetsBody2;
import org.gitnex.tea4j.v2.models.Issue;
import org.gitnex.tea4j.v2.models.IssueConfig;
import org.gitnex.tea4j.v2.models.IssueConfigValidation;
import org.gitnex.tea4j.v2.models.IssueTemplate;
import org.gitnex.tea4j.v2.models.MergePullRequestOption;
import org.gitnex.tea4j.v2.models.MergeUpstreamRequest;
import org.gitnex.tea4j.v2.models.MergeUpstreamResponse;
import org.gitnex.tea4j.v2.models.MigrateRepoOptions;
import org.gitnex.tea4j.v2.models.NewIssuePinsAllowed;
import org.gitnex.tea4j.v2.models.Note;
import org.gitnex.tea4j.v2.models.PullRequest;
import org.gitnex.tea4j.v2.models.PullReview;
import org.gitnex.tea4j.v2.models.PullReviewComment;
import org.gitnex.tea4j.v2.models.PullReviewRequestOptions;
import org.gitnex.tea4j.v2.models.PushMirror;
import org.gitnex.tea4j.v2.models.Reference;
import org.gitnex.tea4j.v2.models.Release;
import org.gitnex.tea4j.v2.models.RepoCollaboratorPermission;
import org.gitnex.tea4j.v2.models.RepoTopicOptions;
import org.gitnex.tea4j.v2.models.Repository;
import org.gitnex.tea4j.v2.models.SearchResults;
import org.gitnex.tea4j.v2.models.Secret;
import org.gitnex.tea4j.v2.models.SubmitPullReviewOptions;
import org.gitnex.tea4j.v2.models.Tag;
import org.gitnex.tea4j.v2.models.TagProtection;
import org.gitnex.tea4j.v2.models.Team;
import org.gitnex.tea4j.v2.models.TopicName;
import org.gitnex.tea4j.v2.models.TopicResponse;
import org.gitnex.tea4j.v2.models.TrackedTime;
import org.gitnex.tea4j.v2.models.TransferRepoOption;
import org.gitnex.tea4j.v2.models.UpdateBranchProtectionPriories;
import org.gitnex.tea4j.v2.models.UpdateBranchRepoOption;
import org.gitnex.tea4j.v2.models.UpdateFileOptions;
import org.gitnex.tea4j.v2.models.UpdateRepoAvatarOption;
import org.gitnex.tea4j.v2.models.UpdateVariableOption;
import org.gitnex.tea4j.v2.models.User;
import org.gitnex.tea4j.v2.models.WatchInfo;
import org.gitnex.tea4j.v2.models.WikiCommitList;
import org.gitnex.tea4j.v2.models.WikiPage;
import org.gitnex.tea4j.v2.models.WikiPageMetaData;
import retrofit2.Call;
import retrofit2.http.*;

public interface RepositoryApi {
  /**
   * Accept a repo transfer
   *
   * @param owner owner of the repo to transfer (required)
   * @param repo name of the repo to transfer (required)
   * @return Call&lt;Repository&gt;
   */
  @POST("repos/{owner}/{repo}/transfer/accept")
  Call<Repository> acceptRepoTransfer(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Create a repository
   *
   * @param body (optional)
   * @return Call&lt;Repository&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("user/repos")
  Call<Repository> createCurrentUserRepo(@retrofit2.http.Body CreateRepoOption body);

  /**
   * Fork a repository
   *
   * @param owner owner of the repo to fork (required)
   * @param repo name of the repo to fork (required)
   * @param body (optional)
   * @return Call&lt;Repository&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/forks")
  Call<Repository> createFork(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Body CreateForkOption body);

  /**
   * Create a repo-level variable
   *
   * @param owner name of the owner (required)
   * @param repo name of the repository (required)
   * @param variablename name of the variable (required)
   * @param body (optional)
   * @return Call&lt;Void&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/actions/variables/{variablename}")
  Call<Void> createRepoVariable(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("variablename") String variablename,
      @retrofit2.http.Body CreateVariableOption body);

  /**
   * Delete a secret in a repository
   *
   * @param owner owner of the repository (required)
   * @param repo name of the repository (required)
   * @param secretname name of the secret (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("repos/{owner}/{repo}/actions/secrets/{secretname}")
  Call<Void> deleteRepoSecret(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("secretname") String secretname);

  /**
   * Delete a repo-level variable
   *
   * @param owner name of the owner (required)
   * @param repo name of the repository (required)
   * @param variablename name of the variable (required)
   * @return Call&lt;ActionVariable&gt;
   */
  @DELETE("repos/{owner}/{repo}/actions/variables/{variablename}")
  Call<ActionVariable> deleteRepoVariable(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("variablename") String variablename);

  /**
   * Create a repository using a template
   *
   * @param templateOwner name of the template repository owner (required)
   * @param templateRepo name of the template repository (required)
   * @param body (optional)
   * @return Call&lt;Repository&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{template_owner}/{template_repo}/generate")
  Call<Repository> generateRepo(
      @retrofit2.http.Path("template_owner") String templateOwner,
      @retrofit2.http.Path("template_repo") String templateRepo,
      @retrofit2.http.Body GenerateRepoOption body);

  /**
   * Gets the tag object of an annotated tag (not lightweight tags)
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param sha sha of the tag. The Git tags API only supports annotated tag objects, not
   *     lightweight tags. (required)
   * @return Call&lt;AnnotatedTag&gt;
   */
  @GET("repos/{owner}/{repo}/git/tags/{sha}")
  Call<AnnotatedTag> getAnnotatedTag(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("sha") String sha);

  /**
   * Gets the blob of a repository.
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param sha sha of the commit (required)
   * @return Call&lt;GitBlobResponse&gt;
   */
  @GET("repos/{owner}/{repo}/git/blobs/{sha}")
  Call<GitBlobResponse> getBlob(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("sha") String sha);

  /**
   * Get a repo-level variable
   *
   * @param owner name of the owner (required)
   * @param repo name of the repository (required)
   * @param variablename name of the variable (required)
   * @return Call&lt;ActionVariable&gt;
   */
  @GET("repos/{owner}/{repo}/actions/variables/{variablename}")
  Call<ActionVariable> getRepoVariable(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("variablename") String variablename);

  /**
   * Get repo-level variables list
   *
   * @param owner name of the owner (required)
   * @param repo name of the repository (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;ActionVariable&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/actions/variables")
  Call<List<ActionVariable>> getRepoVariablesList(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * Gets the tree of a repository.
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param sha sha of the commit (required)
   * @param recursive show all directories and files (optional)
   * @param page page number; the &#x27;truncated&#x27; field in the response will be true if there
   *     are still more items after this page, false if the last page (optional)
   * @param perPage number of items per page (optional)
   * @return Call&lt;GitTreeResponse&gt;
   */
  @GET("repos/{owner}/{repo}/git/trees/{sha}")
  Call<GitTreeResponse> getTree(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("sha") String sha,
      @retrofit2.http.Query("recursive") Boolean recursive,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("per_page") Integer perPage);

  /**
   * List a repository&#x27;s action tasks
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results, default maximum page size is 50 (optional)
   * @return Call&lt;ActionTaskResponse&gt;
   */
  @GET("repos/{owner}/{repo}/actions/tasks")
  Call<ActionTaskResponse> listActionTasks(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List a repository&#x27;s forks
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;Repository&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/forks")
  Call<List<Repository>> listForks(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * Reject a repo transfer
   *
   * @param owner owner of the repo to transfer (required)
   * @param repo name of the repo to transfer (required)
   * @return Call&lt;Repository&gt;
   */
  @POST("repos/{owner}/{repo}/transfer/reject")
  Call<Repository> rejectRepoTransfer(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Add or Update a collaborator to a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param collaborator username of the collaborator to add (required)
   * @param body (optional)
   * @return Call&lt;Void&gt;
   */
  @Headers({"Content-Type:application/json"})
  @PUT("repos/{owner}/{repo}/collaborators/{collaborator}")
  Call<Void> repoAddCollaborator(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("collaborator") String collaborator,
      @retrofit2.http.Body AddCollaboratorOption body);

  /**
   * add a push mirror to the repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param body (optional)
   * @return Call&lt;PushMirror&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/push_mirrors")
  Call<PushMirror> repoAddPushMirror(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Body CreatePushMirrorOption body);

  /**
   * Add a team to a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param team team name (required)
   * @return Call&lt;Void&gt;
   */
  @PUT("repos/{owner}/{repo}/teams/{team}")
  Call<Void> repoAddTeam(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("team") String team);

  /**
   * Add a topic to a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param topic name of the topic to add (required)
   * @return Call&lt;Void&gt;
   */
  @PUT("repos/{owner}/{repo}/topics/{topic}")
  Call<Void> repoAddTopic(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("topic") String topic);

  /**
   * Apply diff patch to repository
   *
   * @param body (required)
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;FileResponse&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/diffpatch")
  Call<FileResponse> repoApplyDiffPatch(
      @retrofit2.http.Body UpdateFileOptions body,
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo);

  /**
   * Cancel the scheduled auto merge for the given pull request
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the pull request to merge (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("repos/{owner}/{repo}/pulls/{index}/merge")
  Call<Void> repoCancelScheduledAutoMerge(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("index") Long index);

  /**
   * Modify multiple files in a repository
   *
   * @param body (required)
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;FilesResponse&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/contents")
  Call<FilesResponse> repoChangeFiles(
      @retrofit2.http.Body ChangeFilesOptions body,
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo);

  /**
   * Check if a user is a collaborator of a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param collaborator username of the collaborator (required)
   * @return Call&lt;Void&gt;
   */
  @GET("repos/{owner}/{repo}/collaborators/{collaborator}")
  Call<Void> repoCheckCollaborator(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("collaborator") String collaborator);

  /**
   * Check if a team is assigned to a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param team team name (required)
   * @return Call&lt;Team&gt;
   */
  @GET("repos/{owner}/{repo}/teams/{team}")
  Call<Team> repoCheckTeam(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("team") String team);

  /**
   * Get commit comparison information
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param basehead compare two branches or commits (required)
   * @return Call&lt;Compare&gt;
   */
  @GET("repos/{owner}/{repo}/compare/{basehead}")
  Call<Compare> repoCompareDiff(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("basehead") String basehead);

  /**
   * Create a branch
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param body (optional)
   * @return Call&lt;Branch&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/branches")
  Call<Branch> repoCreateBranch(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Body CreateBranchRepoOption body);

  /**
   * Create a branch protections for a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param body (optional)
   * @return Call&lt;BranchProtection&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/branch_protections")
  Call<BranchProtection> repoCreateBranchProtection(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Body CreateBranchProtectionOption body);

  /**
   * Create a file in a repository
   *
   * @param body (required)
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param filepath path of the file to create (required)
   * @return Call&lt;FileResponse&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/contents/{filepath}")
  Call<FileResponse> repoCreateFile(
      @retrofit2.http.Body CreateFileOptions body,
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("filepath") String filepath);

  /**
   * Create a hook
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param body (optional)
   * @return Call&lt;Hook&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/hooks")
  Call<Hook> repoCreateHook(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Body CreateHookOption body);

  /**
   * Add a key to a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param body (optional)
   * @return Call&lt;DeployKey&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/keys")
  Call<DeployKey> repoCreateKey(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Body CreateKeyOption body);

  /**
   * Create a pull request
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param body (optional)
   * @return Call&lt;PullRequest&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/pulls")
  Call<PullRequest> repoCreatePullRequest(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Body CreatePullRequestOption body);

  /**
   * Create a review to an pull request
   *
   * @param body (required)
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the pull request (required)
   * @return Call&lt;PullReview&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/pulls/{index}/reviews")
  Call<PullReview> repoCreatePullReview(
      @retrofit2.http.Body CreatePullReviewOptions body,
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("index") Long index);

  /**
   * create review requests for a pull request
   *
   * @param body (required)
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the pull request (required)
   * @return Call&lt;List&lt;PullReview&gt;&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/pulls/{index}/requested_reviewers")
  Call<List<PullReview>> repoCreatePullReviewRequests(
      @retrofit2.http.Body PullReviewRequestOptions body,
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("index") Long index);

  /**
   * Create a release
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param body (optional)
   * @return Call&lt;Release&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/releases")
  Call<Release> repoCreateRelease(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Body CreateReleaseOption body);

  /**
   * Create a release attachment
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id id of the release (required)
   * @param attachment (optional)
   * @param name name of the attachment (optional)
   * @return Call&lt;Attachment&gt;
   */
  @retrofit2.http.Multipart
  @POST("repos/{owner}/{repo}/releases/{id}/assets")
  Call<Attachment> repoCreateReleaseAttachment(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("id") Long id,
      @retrofit2.http.Part("attachment\"; filename=\"attachment") RequestBody attachment,
      @retrofit2.http.Query("name") String name);

  /**
   * Create a release attachment
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id id of the release (required)
   * @param body (optional)
   * @param name name of the attachment (optional)
   * @return Call&lt;Attachment&gt;
   */
  @retrofit2.http.Multipart
  @POST("repos/{owner}/{repo}/releases/{id}/assets")
  Call<Attachment> repoCreateReleaseAttachment(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("id") Long id,
      @retrofit2.http.Body IdAssetsBody2 body,
      @retrofit2.http.Query("name") String name);

  /**
   * Create a commit status
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param sha sha of the commit (required)
   * @param body (optional)
   * @return Call&lt;CommitStatus&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/statuses/{sha}")
  Call<CommitStatus> repoCreateStatus(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("sha") String sha,
      @retrofit2.http.Body CreateStatusOption body);

  /**
   * Create a new git tag in a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param body (optional)
   * @return Call&lt;Tag&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/tags")
  Call<Tag> repoCreateTag(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Body CreateTagOption body);

  /**
   * Create a tag protections for a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param body (optional)
   * @return Call&lt;TagProtection&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/tag_protections")
  Call<TagProtection> repoCreateTagProtection(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Body CreateTagProtectionOption body);

  /**
   * Create a wiki page
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param body (optional)
   * @return Call&lt;WikiPage&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/wiki/new")
  Call<WikiPage> repoCreateWikiPage(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Body CreateWikiPageOptions body);

  /**
   * Delete a repository
   *
   * @param owner owner of the repo to delete (required)
   * @param repo name of the repo to delete (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("repos/{owner}/{repo}")
  Call<Void> repoDelete(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Delete avatar
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("repos/{owner}/{repo}/avatar")
  Call<Void> repoDeleteAvatar(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Delete a specific branch from a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param branch branch to delete (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("repos/{owner}/{repo}/branches/{branch}")
  Call<Void> repoDeleteBranch(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("branch") String branch);

  /**
   * Delete a specific branch protection for the repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param name name of protected branch (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("repos/{owner}/{repo}/branch_protections/{name}")
  Call<Void> repoDeleteBranchProtection(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("name") String name);

  /**
   * Delete a collaborator from a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param collaborator username of the collaborator to delete (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("repos/{owner}/{repo}/collaborators/{collaborator}")
  Call<Void> repoDeleteCollaborator(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("collaborator") String collaborator);

  /**
   * Delete a file in a repository
   *
   * @param body (required)
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param filepath path of the file to delete (required)
   * @return Call&lt;FileDeleteResponse&gt;
   */
  @Headers({"Content-Type:application/json"})
  @DELETE("repos/{owner}/{repo}/contents/{filepath}")
  Call<FileDeleteResponse> repoDeleteFile(
      @retrofit2.http.Body DeleteFileOptions body,
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("filepath") String filepath);

  /**
   * Delete a Git hook in a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id id of the hook to get (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("repos/{owner}/{repo}/hooks/git/{id}")
  Call<Void> repoDeleteGitHook(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("id") String id);

  /**
   * Delete a hook in a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id id of the hook to delete (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("repos/{owner}/{repo}/hooks/{id}")
  Call<Void> repoDeleteHook(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("id") Long id);

  /**
   * Delete a key from a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id id of the key to delete (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("repos/{owner}/{repo}/keys/{id}")
  Call<Void> repoDeleteKey(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("id") Long id);

  /**
   * Delete a specific review from a pull request
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the pull request (required)
   * @param id id of the review (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("repos/{owner}/{repo}/pulls/{index}/reviews/{id}")
  Call<Void> repoDeletePullReview(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("index") Long index,
      @retrofit2.http.Path("id") Long id);

  /**
   * cancel review requests for a pull request
   *
   * @param body (required)
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the pull request (required)
   * @return Call&lt;Void&gt;
   */
  @Headers({"Content-Type:application/json"})
  @DELETE("repos/{owner}/{repo}/pulls/{index}/requested_reviewers")
  Call<Void> repoDeletePullReviewRequests(
      @retrofit2.http.Body PullReviewRequestOptions body,
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("index") Long index);

  /**
   * deletes a push mirror from a repository by remoteName
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param name remote name of the pushMirror (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("repos/{owner}/{repo}/push_mirrors/{name}")
  Call<Void> repoDeletePushMirror(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("name") String name);

  /**
   * Delete a release
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id id of the release to delete (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("repos/{owner}/{repo}/releases/{id}")
  Call<Void> repoDeleteRelease(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("id") Long id);

  /**
   * Delete a release attachment
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id id of the release (required)
   * @param attachmentId id of the attachment to delete (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("repos/{owner}/{repo}/releases/{id}/assets/{attachment_id}")
  Call<Void> repoDeleteReleaseAttachment(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("id") Long id,
      @retrofit2.http.Path("attachment_id") Long attachmentId);

  /**
   * Delete a release by tag name
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param tag tag name of the release to delete (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("repos/{owner}/{repo}/releases/tags/{tag}")
  Call<Void> repoDeleteReleaseByTag(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("tag") String tag);

  /**
   * Delete a repository&#x27;s tag by name
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param tag name of tag to delete (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("repos/{owner}/{repo}/tags/{tag}")
  Call<Void> repoDeleteTag(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("tag") String tag);

  /**
   * Delete a specific tag protection for the repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id id of protected tag (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("repos/{owner}/{repo}/tag_protections/{id}")
  Call<Void> repoDeleteTagProtection(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("id") Integer id);

  /**
   * Delete a team from a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param team team name (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("repos/{owner}/{repo}/teams/{team}")
  Call<Void> repoDeleteTeam(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("team") String team);

  /**
   * Delete a topic from a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param topic name of the topic to delete (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("repos/{owner}/{repo}/topics/{topic}")
  Call<Void> repoDeleteTopic(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("topic") String topic);

  /**
   * Delete a wiki page
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param pageName name of the page (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("repos/{owner}/{repo}/wiki/page/{pageName}")
  Call<Void> repoDeleteWikiPage(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("pageName") String pageName);

  /**
   * Dismiss a review for a pull request
   *
   * @param body (required)
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the pull request (required)
   * @param id id of the review (required)
   * @return Call&lt;PullReview&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/pulls/{index}/reviews/{id}/dismissals")
  Call<PullReview> repoDismissPullReview(
      @retrofit2.http.Body DismissPullReviewOptions body,
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("index") Long index,
      @retrofit2.http.Path("id") Long id);

  /**
   * Get a commit&#x27;s diff or patch
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param sha SHA of the commit to get (required)
   * @param diffType whether the output is diff or patch (required)
   * @return Call&lt;String&gt;
   */
  @GET("repos/{owner}/{repo}/git/commits/{sha}.{diffType}")
  Call<String> repoDownloadCommitDiffOrPatch(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("sha") String sha,
      @retrofit2.http.Path("diffType") String diffType);

  /**
   * Get a pull request diff or patch
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the pull request to get (required)
   * @param diffType whether the output is diff or patch (required)
   * @param binary whether to include binary file changes. if true, the diff is applicable with
   *     &#x60;git apply&#x60; (optional)
   * @return Call&lt;String&gt;
   */
  @GET("repos/{owner}/{repo}/pulls/{index}.{diffType}")
  Call<String> repoDownloadPullDiffOrPatch(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("index") Long index,
      @retrofit2.http.Path("diffType") String diffType,
      @retrofit2.http.Query("binary") Boolean binary);

  /**
   * Edit a repository&#x27;s properties. Only fields that are set will be changed.
   *
   * @param owner owner of the repo to edit (required)
   * @param repo name of the repo to edit (required)
   * @param body Properties of a repo that you can edit (optional)
   * @return Call&lt;Repository&gt;
   */
  @Headers({"Content-Type:application/json"})
  @PATCH("repos/{owner}/{repo}")
  Call<Repository> repoEdit(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Body EditRepoOption body);

  /**
   * Edit a branch protections for a repository. Only fields that are set will be changed
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param name name of protected branch (required)
   * @param body (optional)
   * @return Call&lt;BranchProtection&gt;
   */
  @Headers({"Content-Type:application/json"})
  @PATCH("repos/{owner}/{repo}/branch_protections/{name}")
  Call<BranchProtection> repoEditBranchProtection(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("name") String name,
      @retrofit2.http.Body EditBranchProtectionOption body);

  /**
   * Edit a Git hook in a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id id of the hook to get (required)
   * @param body (optional)
   * @return Call&lt;GitHook&gt;
   */
  @Headers({"Content-Type:application/json"})
  @PATCH("repos/{owner}/{repo}/hooks/git/{id}")
  Call<GitHook> repoEditGitHook(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("id") String id,
      @retrofit2.http.Body EditGitHookOption body);

  /**
   * Edit a hook in a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id index of the hook (required)
   * @param body (optional)
   * @return Call&lt;Hook&gt;
   */
  @Headers({"Content-Type:application/json"})
  @PATCH("repos/{owner}/{repo}/hooks/{id}")
  Call<Hook> repoEditHook(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("id") Long id,
      @retrofit2.http.Body EditHookOption body);

  /**
   * Update a pull request. If using deadline only the date will be taken into account, and time of
   * day ignored.
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the pull request to edit (required)
   * @param body (optional)
   * @return Call&lt;PullRequest&gt;
   */
  @Headers({"Content-Type:application/json"})
  @PATCH("repos/{owner}/{repo}/pulls/{index}")
  Call<PullRequest> repoEditPullRequest(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("index") Long index,
      @retrofit2.http.Body EditPullRequestOption body);

  /**
   * Update a release
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id id of the release to edit (required)
   * @param body (optional)
   * @return Call&lt;Release&gt;
   */
  @Headers({"Content-Type:application/json"})
  @PATCH("repos/{owner}/{repo}/releases/{id}")
  Call<Release> repoEditRelease(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("id") Long id,
      @retrofit2.http.Body EditReleaseOption body);

  /**
   * Edit a release attachment
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id id of the release (required)
   * @param attachmentId id of the attachment to edit (required)
   * @param body (optional)
   * @return Call&lt;Attachment&gt;
   */
  @Headers({"Content-Type:application/json"})
  @PATCH("repos/{owner}/{repo}/releases/{id}/assets/{attachment_id}")
  Call<Attachment> repoEditReleaseAttachment(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("id") Long id,
      @retrofit2.http.Path("attachment_id") Long attachmentId,
      @retrofit2.http.Body EditAttachmentOptions body);

  /**
   * Edit a tag protections for a repository. Only fields that are set will be changed
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id id of protected tag (required)
   * @param body (optional)
   * @return Call&lt;TagProtection&gt;
   */
  @Headers({"Content-Type:application/json"})
  @PATCH("repos/{owner}/{repo}/tag_protections/{id}")
  Call<TagProtection> repoEditTagProtection(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("id") Integer id,
      @retrofit2.http.Body EditTagProtectionOption body);

  /**
   * Edit a wiki page
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param pageName name of the page (required)
   * @param body (optional)
   * @return Call&lt;WikiPage&gt;
   */
  @Headers({"Content-Type:application/json"})
  @PATCH("repos/{owner}/{repo}/wiki/page/{pageName}")
  Call<WikiPage> repoEditWikiPage(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("pageName") String pageName,
      @retrofit2.http.Body CreateWikiPageOptions body);

  /**
   * Get a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;Repository&gt;
   */
  @GET("repos/{owner}/{repo}")
  Call<Repository> repoGet(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Get a list of all commits from a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param sha SHA or branch to start listing commits from (usually &#x27;master&#x27;) (optional)
   * @param path filepath of a file/dir (optional)
   * @param stat include diff stats for every commit (disable for speedup, default &#x27;true&#x27;)
   *     (optional)
   * @param verification include verification for every commit (disable for speedup, default
   *     &#x27;true&#x27;) (optional)
   * @param files include a list of affected files for every commit (disable for speedup, default
   *     &#x27;true&#x27;) (optional)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (ignored if used with &#x27;path&#x27;) (optional)
   * @param not commits that match the given specifier will not be listed. (optional)
   * @return Call&lt;List&lt;Commit&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/commits")
  Call<List<Commit>> repoGetAllCommits(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Query("sha") String sha,
      @retrofit2.http.Query("path") String path,
      @retrofit2.http.Query("stat") Boolean stat,
      @retrofit2.http.Query("verification") Boolean verification,
      @retrofit2.http.Query("files") Boolean files,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit,
      @retrofit2.http.Query("not") String not);

  /**
   * Get an archive of a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param archive the git reference for download with attached archive format (e.g. master.zip)
   *     (required)
   * @return Call&lt;Void&gt;
   */
  @GET("repos/{owner}/{repo}/archive/{archive}")
  Call<Void> repoGetArchive(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("archive") String archive);

  /**
   * Return all users that have write access and can be assigned to issues
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;List&lt;User&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/assignees")
  Call<List<User>> repoGetAssignees(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Retrieve a specific branch from a repository, including its effective branch protection
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param branch branch to get (required)
   * @return Call&lt;Branch&gt;
   */
  @GET("repos/{owner}/{repo}/branches/{branch}")
  Call<Branch> repoGetBranch(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("branch") String branch);

  /**
   * Get a specific branch protection for the repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param name name of protected branch (required)
   * @return Call&lt;BranchProtection&gt;
   */
  @GET("repos/{owner}/{repo}/branch_protections/{name}")
  Call<BranchProtection> repoGetBranchProtection(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("name") String name);

  /**
   * Get a repository by id
   *
   * @param id id of the repo to get (required)
   * @return Call&lt;Repository&gt;
   */
  @GET("repositories/{id}")
  Call<Repository> repoGetByID(@retrofit2.http.Path("id") Long id);

  /**
   * Get a commit&#x27;s combined status, by branch/tag/commit reference
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param ref name of branch/tag/commit (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;CombinedStatus&gt;
   */
  @GET("repos/{owner}/{repo}/commits/{ref}/status")
  Call<CombinedStatus> repoGetCombinedStatusByRef(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("ref") String ref,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * Get the merged pull request of the commit
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param sha SHA of the commit to get (required)
   * @return Call&lt;PullRequest&gt;
   */
  @GET("repos/{owner}/{repo}/commits/{sha}/pull")
  Call<PullRequest> repoGetCommitPullRequest(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("sha") String sha);

  /**
   * Gets the metadata and contents (if a file) of an entry in a repository, or a list of entries if
   * a dir
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param filepath path of the dir, file, symlink or submodule in the repo (required)
   * @param ref The name of the commit/branch/tag. Default the repository’s default branch (usually
   *     master) (optional)
   * @return Call&lt;ContentsResponse&gt;
   */
  @GET("repos/{owner}/{repo}/contents/{filepath}")
  Call<ContentsResponse> repoGetContents(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("filepath") String filepath,
      @retrofit2.http.Query("ref") String ref);

  /**
   * Gets the metadata of all the entries of the root dir
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param ref The name of the commit/branch/tag. Default the repository’s default branch (usually
   *     master) (optional)
   * @return Call&lt;List&lt;ContentsResponse&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/contents")
  Call<List<ContentsResponse>> repoGetContentsList(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Query("ref") String ref);

  /**
   * Get the EditorConfig definitions of a file in a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param filepath filepath of file to get (required)
   * @param ref The name of the commit/branch/tag. Default the repository’s default branch (usually
   *     master) (optional)
   * @return Call&lt;Void&gt;
   */
  @GET("repos/{owner}/{repo}/editorconfig/{filepath}")
  Call<Void> repoGetEditorConfig(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("filepath") String filepath,
      @retrofit2.http.Query("ref") String ref);

  /**
   * Get a Git hook
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id id of the hook to get (required)
   * @return Call&lt;GitHook&gt;
   */
  @GET("repos/{owner}/{repo}/hooks/git/{id}")
  Call<GitHook> repoGetGitHook(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("id") String id);

  /**
   * Get a hook
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id id of the hook to get (required)
   * @return Call&lt;Hook&gt;
   */
  @GET("repos/{owner}/{repo}/hooks/{id}")
  Call<Hook> repoGetHook(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("id") Long id);

  /**
   * Returns the issue config for a repo
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;IssueConfig&gt;
   */
  @GET("repos/{owner}/{repo}/issue_config")
  Call<IssueConfig> repoGetIssueConfig(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Get available issue templates for a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;List&lt;IssueTemplate&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/issue_templates")
  Call<List<IssueTemplate>> repoGetIssueTemplates(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Get a repository&#x27;s key by id
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id id of the key to get (required)
   * @return Call&lt;DeployKey&gt;
   */
  @GET("repos/{owner}/{repo}/keys/{id}")
  Call<DeployKey> repoGetKey(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("id") Long id);

  /**
   * Get languages and number of bytes of code written
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;Map&lt;String, Long&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/languages")
  Call<Map<String, Long>> repoGetLanguages(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Gets the most recent non-prerelease, non-draft release of a repository, sorted by created_at
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;Release&gt;
   */
  @GET("repos/{owner}/{repo}/releases/latest")
  Call<Release> repoGetLatestRelease(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Get repo licenses
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;List&lt;String&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/licenses")
  Call<List<String>> repoGetLicenses(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Get a note corresponding to a single commit from a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param sha a git ref or commit sha (required)
   * @param verification include verification for every commit (disable for speedup, default
   *     &#x27;true&#x27;) (optional)
   * @param files include a list of affected files for every commit (disable for speedup, default
   *     &#x27;true&#x27;) (optional)
   * @return Call&lt;Note&gt;
   */
  @GET("repos/{owner}/{repo}/git/notes/{sha}")
  Call<Note> repoGetNote(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("sha") String sha,
      @retrofit2.http.Query("verification") Boolean verification,
      @retrofit2.http.Query("files") Boolean files);

  /**
   * Get a pull request
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the pull request to get (required)
   * @return Call&lt;PullRequest&gt;
   */
  @GET("repos/{owner}/{repo}/pulls/{index}")
  Call<PullRequest> repoGetPullRequest(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("index") Long index);

  /**
   * Get a pull request by base and head
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param base base of the pull request to get (required)
   * @param head head of the pull request to get (required)
   * @return Call&lt;PullRequest&gt;
   */
  @GET("repos/{owner}/{repo}/pulls/{base}/{head}")
  Call<PullRequest> repoGetPullRequestByBaseHead(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("base") String base,
      @retrofit2.http.Path("head") String head);

  /**
   * Get commits for a pull request
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the pull request to get (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @param verification include verification for every commit (disable for speedup, default
   *     &#x27;true&#x27;) (optional)
   * @param files include a list of affected files for every commit (disable for speedup, default
   *     &#x27;true&#x27;) (optional)
   * @return Call&lt;List&lt;Commit&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/pulls/{index}/commits")
  Call<List<Commit>> repoGetPullRequestCommits(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("index") Long index,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit,
      @retrofit2.http.Query("verification") Boolean verification,
      @retrofit2.http.Query("files") Boolean files);

  /**
   * Get changed files for a pull request
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the pull request to get (required)
   * @param skipTo skip to given file (optional)
   * @param whitespace whitespace behavior (optional)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;ChangedFile&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/pulls/{index}/files")
  Call<List<ChangedFile>> repoGetPullRequestFiles(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("index") Long index,
      @retrofit2.http.Query("skip-to") String skipTo,
      @retrofit2.http.Query("whitespace") String whitespace,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * Get a specific review for a pull request
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the pull request (required)
   * @param id id of the review (required)
   * @return Call&lt;PullReview&gt;
   */
  @GET("repos/{owner}/{repo}/pulls/{index}/reviews/{id}")
  Call<PullReview> repoGetPullReview(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("index") Long index,
      @retrofit2.http.Path("id") Long id);

  /**
   * Get a specific review for a pull request
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the pull request (required)
   * @param id id of the review (required)
   * @return Call&lt;List&lt;PullReviewComment&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/pulls/{index}/reviews/{id}/comments")
  Call<List<PullReviewComment>> repoGetPullReviewComments(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("index") Long index,
      @retrofit2.http.Path("id") Long id);

  /**
   * Get push mirror of the repository by remoteName
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param name remote name of push mirror (required)
   * @return Call&lt;PushMirror&gt;
   */
  @GET("repos/{owner}/{repo}/push_mirrors/{name}")
  Call<PushMirror> repoGetPushMirrorByRemoteName(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("name") String name);

  /**
   * Get a file from a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param filepath path of the file to get, it should be \&quot;{ref}/{filepath}\&quot;. If there
   *     is no ref could be inferred, it will be treated as the default branch (required)
   * @param ref The name of the commit/branch/tag. Default the repository’s default branch
   *     (optional)
   * @return Call&lt;File&gt;
   */
  @GET("repos/{owner}/{repo}/raw/{filepath}")
  Call<File> repoGetRawFile(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("filepath") String filepath,
      @retrofit2.http.Query("ref") String ref);

  /**
   * Get a file or it&#x27;s LFS object from a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param filepath path of the file to get, it should be \&quot;{ref}/{filepath}\&quot;. If there
   *     is no ref could be inferred, it will be treated as the default branch (required)
   * @param ref The name of the commit/branch/tag. Default the repository’s default branch
   *     (optional)
   * @return Call&lt;File&gt;
   */
  @GET("repos/{owner}/{repo}/media/{filepath}")
  Call<File> repoGetRawFileOrLFS(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("filepath") String filepath,
      @retrofit2.http.Query("ref") String ref);

  /**
   * Get a release
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id id of the release to get (required)
   * @return Call&lt;Release&gt;
   */
  @GET("repos/{owner}/{repo}/releases/{id}")
  Call<Release> repoGetRelease(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("id") Long id);

  /**
   * Get a release attachment
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id id of the release (required)
   * @param attachmentId id of the attachment to get (required)
   * @return Call&lt;Attachment&gt;
   */
  @GET("repos/{owner}/{repo}/releases/{id}/assets/{attachment_id}")
  Call<Attachment> repoGetReleaseAttachment(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("id") Long id,
      @retrofit2.http.Path("attachment_id") Long attachmentId);

  /**
   * Get a release by tag name
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param tag tag name of the release to get (required)
   * @return Call&lt;Release&gt;
   */
  @GET("repos/{owner}/{repo}/releases/tags/{tag}")
  Call<Release> repoGetReleaseByTag(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("tag") String tag);

  /**
   * Get repository permissions for a user
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param collaborator username of the collaborator (required)
   * @return Call&lt;RepoCollaboratorPermission&gt;
   */
  @GET("repos/{owner}/{repo}/collaborators/{collaborator}/permission")
  Call<RepoCollaboratorPermission> repoGetRepoPermissions(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("collaborator") String collaborator);

  /**
   * Return all users that can be requested to review in this repo
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;List&lt;User&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/reviewers")
  Call<List<User>> repoGetReviewers(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Get a repository&#x27;s actions runner registration token
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;Void&gt;
   */
  @GET("repos/{owner}/{repo}/actions/runners/registration-token")
  Call<Void> repoGetRunnerRegistrationToken(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Get a single commit from a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param sha a git ref or commit sha (required)
   * @param stat include diff stats for every commit (disable for speedup, default &#x27;true&#x27;)
   *     (optional)
   * @param verification include verification for every commit (disable for speedup, default
   *     &#x27;true&#x27;) (optional)
   * @param files include a list of affected files for every commit (disable for speedup, default
   *     &#x27;true&#x27;) (optional)
   * @return Call&lt;Commit&gt;
   */
  @GET("repos/{owner}/{repo}/git/commits/{sha}")
  Call<Commit> repoGetSingleCommit(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("sha") String sha,
      @retrofit2.http.Query("stat") Boolean stat,
      @retrofit2.http.Query("verification") Boolean verification,
      @retrofit2.http.Query("files") Boolean files);

  /**
   * Get the tag of a repository by tag name
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param tag name of tag (required)
   * @return Call&lt;Tag&gt;
   */
  @GET("repos/{owner}/{repo}/tags/{tag}")
  Call<Tag> repoGetTag(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("tag") String tag);

  /**
   * Get a specific tag protection for the repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id id of the tag protect to get (required)
   * @return Call&lt;TagProtection&gt;
   */
  @GET("repos/{owner}/{repo}/tag_protections/{id}")
  Call<TagProtection> repoGetTagProtection(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("id") Integer id);

  /**
   * Get a wiki page
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param pageName name of the page (required)
   * @return Call&lt;WikiPage&gt;
   */
  @GET("repos/{owner}/{repo}/wiki/page/{pageName}")
  Call<WikiPage> repoGetWikiPage(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("pageName") String pageName);

  /**
   * Get revisions of a wiki page
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param pageName name of the page (required)
   * @param page page number of results to return (1-based) (optional)
   * @return Call&lt;WikiCommitList&gt;
   */
  @GET("repos/{owner}/{repo}/wiki/revisions/{pageName}")
  Call<WikiCommitList> repoGetWikiPageRevisions(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("pageName") String pageName,
      @retrofit2.http.Query("page") Integer page);

  /**
   * Get all wiki pages
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;WikiPageMetaData&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/wiki/pages")
  Call<List<WikiPageMetaData>> repoGetWikiPages(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List an repo&#x27;s actions secrets
   *
   * @param owner owner of the repository (required)
   * @param repo name of the repository (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;Secret&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/actions/secrets")
  Call<List<Secret>> repoListActionsSecrets(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List a repository&#x27;s activity feeds
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param date the date of the activities to be found (optional)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;Activity&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/activities/feeds")
  Call<List<Activity>> repoListActivityFeeds(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Query("date") Date date,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * Get specified ref or filtered repository&#x27;s refs
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;List&lt;Reference&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/git/refs")
  Call<List<Reference>> repoListAllGitRefs(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * List branch protections for a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;List&lt;BranchProtection&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/branch_protections")
  Call<List<BranchProtection>> repoListBranchProtection(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * List a repository&#x27;s branches
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;Branch&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/branches")
  Call<List<Branch>> repoListBranches(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List a repository&#x27;s collaborators
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;User&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/collaborators")
  Call<List<User>> repoListCollaborators(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List the Git hooks in a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;List&lt;GitHook&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/hooks/git")
  Call<List<GitHook>> repoListGitHooks(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Get specified ref or filtered repository&#x27;s refs
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param ref part or full name of the ref (required)
   * @return Call&lt;List&lt;Reference&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/git/refs/{ref}")
  Call<List<Reference>> repoListGitRefs(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("ref") String ref);

  /**
   * List the hooks in a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;Hook&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/hooks")
  Call<List<Hook>> repoListHooks(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List a repository&#x27;s keys
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param keyId the key_id to search for (optional)
   * @param fingerprint fingerprint of the key (optional)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;DeployKey&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/keys")
  Call<List<DeployKey>> repoListKeys(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Query("key_id") Integer keyId,
      @retrofit2.http.Query("fingerprint") String fingerprint,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List a repo&#x27;s pinned issues
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;List&lt;Issue&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/issues/pinned")
  Call<List<Issue>> repoListPinnedIssues(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * List a repo&#x27;s pinned pull requests
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;List&lt;PullRequest&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/pulls/pinned")
  Call<List<PullRequest>> repoListPinnedPullRequests(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * List a repo&#x27;s pull requests
   *
   * @param owner Owner of the repo (required)
   * @param repo Name of the repo (required)
   * @param state State of pull request (optional, default to open)
   * @param sort Type of sort (optional)
   * @param milestone ID of the milestone (optional)
   * @param labels Label IDs (optional)
   * @param poster Filter by pull request author (optional)
   * @param page Page number of results to return (1-based) (optional, default to 1)
   * @param limit Page size of results (optional)
   * @return Call&lt;List&lt;PullRequest&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/pulls")
  Call<List<PullRequest>> repoListPullRequests(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Query("state") String state,
      @retrofit2.http.Query("sort") String sort,
      @retrofit2.http.Query("milestone") Long milestone,
      @retrofit2.http.Query("labels") List<Long> labels,
      @retrofit2.http.Query("poster") String poster,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List all reviews for a pull request
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the pull request (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;PullReview&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/pulls/{index}/reviews")
  Call<List<PullReview>> repoListPullReviews(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("index") Long index,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * Get all push mirrors of the repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;PushMirror&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/push_mirrors")
  Call<List<PushMirror>> repoListPushMirrors(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List release&#x27;s attachments
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id id of the release (required)
   * @return Call&lt;List&lt;Attachment&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/releases/{id}/assets")
  Call<List<Attachment>> repoListReleaseAttachments(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("id") Long id);

  /**
   * List a repo&#x27;s releases
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param draft filter (exclude / include) drafts, if you dont have repo write access none will
   *     show (optional)
   * @param preRelease filter (exclude / include) pre-releases (optional)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;Release&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/releases")
  Call<List<Release>> repoListReleases(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Query("draft") Boolean draft,
      @retrofit2.http.Query("pre-release") Boolean preRelease,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List a repo&#x27;s stargazers
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;User&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/stargazers")
  Call<List<User>> repoListStargazers(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * Get a commit&#x27;s statuses
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param sha sha of the commit (required)
   * @param sort type of sort (optional)
   * @param state type of state (optional)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;CommitStatus&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/statuses/{sha}")
  Call<List<CommitStatus>> repoListStatuses(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("sha") String sha,
      @retrofit2.http.Query("sort") String sort,
      @retrofit2.http.Query("state") String state,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * Get a commit&#x27;s statuses, by branch/tag/commit reference
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param ref name of branch/tag/commit (required)
   * @param sort type of sort (optional)
   * @param state type of state (optional)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;CommitStatus&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/commits/{ref}/statuses")
  Call<List<CommitStatus>> repoListStatusesByRef(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("ref") String ref,
      @retrofit2.http.Query("sort") String sort,
      @retrofit2.http.Query("state") String state,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List a repo&#x27;s watchers
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;User&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/subscribers")
  Call<List<User>> repoListSubscribers(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List tag protections for a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;List&lt;TagProtection&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/tag_protections")
  Call<List<TagProtection>> repoListTagProtection(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * List a repository&#x27;s tags
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results, default maximum page size is 50 (optional)
   * @return Call&lt;List&lt;Tag&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/tags")
  Call<List<Tag>> repoListTags(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List a repository&#x27;s teams
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;List&lt;Team&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/teams")
  Call<List<Team>> repoListTeams(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Get list of topics that a repository has
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;TopicName&gt;
   */
  @GET("repos/{owner}/{repo}/topics")
  Call<TopicName> repoListTopics(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * Merge a pull request
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the pull request to merge (required)
   * @param body (optional)
   * @return Call&lt;Void&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/pulls/{index}/merge")
  Call<Void> repoMergePullRequest(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("index") Long index,
      @retrofit2.http.Body MergePullRequestOption body);

  /**
   * Merge a branch from upstream
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param body (optional)
   * @return Call&lt;MergeUpstreamResponse&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/merge-upstream")
  Call<MergeUpstreamResponse> repoMergeUpstream(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Body MergeUpstreamRequest body);

  /**
   * Migrate a remote git repository
   *
   * @param body (optional)
   * @return Call&lt;Repository&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/migrate")
  Call<Repository> repoMigrate(@retrofit2.http.Body MigrateRepoOptions body);

  /**
   * Sync a mirrored repository
   *
   * @param owner owner of the repo to sync (required)
   * @param repo name of the repo to sync (required)
   * @return Call&lt;Void&gt;
   */
  @POST("repos/{owner}/{repo}/mirror-sync")
  Call<Void> repoMirrorSync(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Returns if new Issue Pins are allowed
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;NewIssuePinsAllowed&gt;
   */
  @GET("repos/{owner}/{repo}/new_pin_allowed")
  Call<NewIssuePinsAllowed> repoNewPinAllowed(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Check if a pull request has been merged
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the pull request (required)
   * @return Call&lt;Void&gt;
   */
  @GET("repos/{owner}/{repo}/pulls/{index}/merge")
  Call<Void> repoPullRequestIsMerged(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("index") Long index);

  /**
   * Sync all push mirrored repository
   *
   * @param owner owner of the repo to sync (required)
   * @param repo name of the repo to sync (required)
   * @return Call&lt;Void&gt;
   */
  @POST("repos/{owner}/{repo}/push_mirrors-sync")
  Call<Void> repoPushMirrorSync(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Search for repositories
   *
   * @param q keyword (optional)
   * @param topic Limit search to repositories with keyword as topic (optional)
   * @param includeDesc include search of keyword within repository description (optional)
   * @param uid search only for repos that the user with the given id owns or contributes to
   *     (optional)
   * @param priorityOwnerId repo owner to prioritize in the results (optional)
   * @param teamId search only for repos that belong to the given team id (optional)
   * @param starredBy search only for repos that the user with the given id has starred (optional)
   * @param _private include private repositories this user has access to (defaults to true)
   *     (optional)
   * @param isPrivate show only pubic, private or all repositories (defaults to all) (optional)
   * @param template include template repositories this user has access to (defaults to true)
   *     (optional)
   * @param archived show only archived, non-archived or all repositories (defaults to all)
   *     (optional)
   * @param mode type of repository to search for. Supported values are \&quot;fork\&quot;,
   *     \&quot;source\&quot;, \&quot;mirror\&quot; and \&quot;collaborative\&quot; (optional)
   * @param exclusive if &#x60;uid&#x60; is given, search only for repos that the user owns
   *     (optional)
   * @param sort sort repos by attribute. Supported values are \&quot;alpha\&quot;,
   *     \&quot;created\&quot;, \&quot;updated\&quot;, \&quot;size\&quot;, \&quot;git_size\&quot;,
   *     \&quot;lfs_size\&quot;, \&quot;stars\&quot;, \&quot;forks\&quot; and \&quot;id\&quot;.
   *     Default is \&quot;alpha\&quot; (optional)
   * @param order sort order, either \&quot;asc\&quot; (ascending) or \&quot;desc\&quot;
   *     (descending). Default is \&quot;asc\&quot;, ignored if \&quot;sort\&quot; is not specified.
   *     (optional)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;SearchResults&gt;
   */
  @GET("repos/search")
  Call<SearchResults> repoSearch(
      @retrofit2.http.Query("q") String q,
      @retrofit2.http.Query("topic") Boolean topic,
      @retrofit2.http.Query("includeDesc") Boolean includeDesc,
      @retrofit2.http.Query("uid") Long uid,
      @retrofit2.http.Query("priority_owner_id") Long priorityOwnerId,
      @retrofit2.http.Query("team_id") Long teamId,
      @retrofit2.http.Query("starredBy") Long starredBy,
      @retrofit2.http.Query("private") Boolean _private,
      @retrofit2.http.Query("is_private") Boolean isPrivate,
      @retrofit2.http.Query("template") Boolean template,
      @retrofit2.http.Query("archived") Boolean archived,
      @retrofit2.http.Query("mode") String mode,
      @retrofit2.http.Query("exclusive") Boolean exclusive,
      @retrofit2.http.Query("sort") String sort,
      @retrofit2.http.Query("order") String order,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * Get signing-key.gpg for given repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;String&gt;
   */
  @GET("repos/{owner}/{repo}/signing-key.gpg")
  Call<String> repoSigningKey(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Submit a pending review to an pull request
   *
   * @param body (required)
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the pull request (required)
   * @param id id of the review (required)
   * @return Call&lt;PullReview&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/pulls/{index}/reviews/{id}")
  Call<PullReview> repoSubmitPullReview(
      @retrofit2.http.Body SubmitPullReviewOptions body,
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("index") Long index,
      @retrofit2.http.Path("id") Long id);

  /**
   * Test a push webhook
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id id of the hook to test (required)
   * @param ref The name of the commit/branch/tag, indicates which commit will be loaded to the
   *     webhook payload. (optional)
   * @return Call&lt;Void&gt;
   */
  @POST("repos/{owner}/{repo}/hooks/{id}/tests")
  Call<Void> repoTestHook(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("id") Long id,
      @retrofit2.http.Query("ref") String ref);

  /**
   * List a repo&#x27;s tracked times
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param user optional filter by user (available for issue managers) (optional)
   * @param since Only show times updated after the given time. This is a timestamp in RFC 3339
   *     format (optional)
   * @param before Only show times updated before the given time. This is a timestamp in RFC 3339
   *     format (optional)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;TrackedTime&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/times")
  Call<List<TrackedTime>> repoTrackedTimes(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Query("user") String user,
      @retrofit2.http.Query("since") Date since,
      @retrofit2.http.Query("before") Date before,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * Transfer a repo ownership
   *
   * @param body Transfer Options (required)
   * @param owner owner of the repo to transfer (required)
   * @param repo name of the repo to transfer (required)
   * @return Call&lt;Repository&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/transfer")
  Call<Repository> repoTransfer(
      @retrofit2.http.Body TransferRepoOption body,
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo);

  /**
   * Cancel to dismiss a review for a pull request
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the pull request (required)
   * @param id id of the review (required)
   * @return Call&lt;PullReview&gt;
   */
  @POST("repos/{owner}/{repo}/pulls/{index}/reviews/{id}/undismissals")
  Call<PullReview> repoUnDismissPullReview(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("index") Long index,
      @retrofit2.http.Path("id") Long id);

  /**
   * Update avatar
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param body (optional)
   * @return Call&lt;Void&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/avatar")
  Call<Void> repoUpdateAvatar(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Body UpdateRepoAvatarOption body);

  /**
   * Update a branch
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param branch name of the branch (required)
   * @param body (optional)
   * @return Call&lt;Void&gt;
   */
  @Headers({"Content-Type:application/json"})
  @PATCH("repos/{owner}/{repo}/branches/{branch}")
  Call<Void> repoUpdateBranch(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("branch") String branch,
      @retrofit2.http.Body UpdateBranchRepoOption body);

  /**
   * Update the priorities of branch protections for a repository.
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param body (optional)
   * @return Call&lt;Void&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("repos/{owner}/{repo}/branch_protections/priority")
  Call<Void> repoUpdateBranchProtectionPriories(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Body UpdateBranchProtectionPriories body);

  /**
   * Update a file in a repository
   *
   * @param body (required)
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param filepath path of the file to update (required)
   * @return Call&lt;FileResponse&gt;
   */
  @Headers({"Content-Type:application/json"})
  @PUT("repos/{owner}/{repo}/contents/{filepath}")
  Call<FileResponse> repoUpdateFile(
      @retrofit2.http.Body UpdateFileOptions body,
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("filepath") String filepath);

  /**
   * Merge PR&#x27;s baseBranch into headBranch
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the pull request to get (required)
   * @param style how to update pull request (optional)
   * @return Call&lt;Void&gt;
   */
  @POST("repos/{owner}/{repo}/pulls/{index}/update")
  Call<Void> repoUpdatePullRequest(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("index") Long index,
      @retrofit2.http.Query("style") String style);

  /**
   * Replace list of topics for a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param body (optional)
   * @return Call&lt;Void&gt;
   */
  @Headers({"Content-Type:application/json"})
  @PUT("repos/{owner}/{repo}/topics")
  Call<Void> repoUpdateTopics(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Body RepoTopicOptions body);

  /**
   * Returns the validation information for a issue config
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;IssueConfigValidation&gt;
   */
  @GET("repos/{owner}/{repo}/issue_config/validate")
  Call<IssueConfigValidation> repoValidateIssueConfig(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * search topics via keyword
   *
   * @param q keywords to search (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;TopicResponse&gt;&gt;
   */
  @GET("topics/search")
  Call<List<TopicResponse>> topicSearch(
      @retrofit2.http.Query("q") String q,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * Create or Update a secret value in a repository
   *
   * @param owner owner of the repository (required)
   * @param repo name of the repository (required)
   * @param secretname name of the secret (required)
   * @param body (optional)
   * @return Call&lt;Void&gt;
   */
  @Headers({"Content-Type:application/json"})
  @PUT("repos/{owner}/{repo}/actions/secrets/{secretname}")
  Call<Void> updateRepoSecret(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("secretname") String secretname,
      @retrofit2.http.Body CreateOrUpdateSecretOption body);

  /**
   * Update a repo-level variable
   *
   * @param owner name of the owner (required)
   * @param repo name of the repository (required)
   * @param variablename name of the variable (required)
   * @param body (optional)
   * @return Call&lt;Void&gt;
   */
  @Headers({"Content-Type:application/json"})
  @PUT("repos/{owner}/{repo}/actions/variables/{variablename}")
  Call<Void> updateRepoVariable(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("variablename") String variablename,
      @retrofit2.http.Body UpdateVariableOption body);

  /**
   * Check if the current user is watching a repo
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;WatchInfo&gt;
   */
  @GET("repos/{owner}/{repo}/subscription")
  Call<WatchInfo> userCurrentCheckSubscription(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Unwatch a repo
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("repos/{owner}/{repo}/subscription")
  Call<Void> userCurrentDeleteSubscription(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Watch a repo
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;WatchInfo&gt;
   */
  @PUT("repos/{owner}/{repo}/subscription")
  Call<WatchInfo> userCurrentPutSubscription(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * List a user&#x27;s tracked times in a repo
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param user username of user (required)
   * @return Call&lt;List&lt;TrackedTime&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/times/{user}")
  Call<List<TrackedTime>> userTrackedTimes(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("user") String user);
}
