package org.gitnex.tea4j.v2.apis;

import java.util.List;
import org.gitnex.tea4j.v2.CollectionFormats.*;
import org.gitnex.tea4j.v2.models.ModelPackage;
import org.gitnex.tea4j.v2.models.PackageFile;
import retrofit2.Call;
import retrofit2.http.*;

public interface PackageApi {
  /**
   * Delete a package
   *
   * @param owner owner of the package (required)
   * @param type type of the package (required)
   * @param name name of the package (required)
   * @param version version of the package (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("packages/{owner}/{type}/{name}/{version}")
  Call<Void> deletePackage(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("type") String type,
      @retrofit2.http.Path("name") String name,
      @retrofit2.http.Path("version") String version);

  /**
   * Gets a package
   *
   * @param owner owner of the package (required)
   * @param type type of the package (required)
   * @param name name of the package (required)
   * @param version version of the package (required)
   * @return Call&lt;ModelPackage&gt;
   */
  @GET("packages/{owner}/{type}/{name}/{version}")
  Call<ModelPackage> getPackage(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("type") String type,
      @retrofit2.http.Path("name") String name,
      @retrofit2.http.Path("version") String version);

  /**
   * Gets all files of a package
   *
   * @param owner owner of the package (required)
   * @param type type of the package (required)
   * @param name name of the package (required)
   * @param version version of the package (required)
   * @return Call&lt;List&lt;PackageFile&gt;&gt;
   */
  @GET("packages/{owner}/{type}/{name}/{version}/files")
  Call<List<PackageFile>> listPackageFiles(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("type") String type,
      @retrofit2.http.Path("name") String name,
      @retrofit2.http.Path("version") String version);

  /**
   * Gets all packages of an owner
   *
   * @param owner owner of the packages (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @param type package type filter (optional)
   * @param q name filter (optional)
   * @return Call&lt;List&lt;ModelPackage&gt;&gt;
   */
  @GET("packages/{owner}")
  Call<List<ModelPackage>> listPackages(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit,
      @retrofit2.http.Query("type") String type,
      @retrofit2.http.Query("q") String q);
}
