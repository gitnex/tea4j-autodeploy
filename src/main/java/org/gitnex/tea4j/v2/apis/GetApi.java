package org.gitnex.tea4j.v2.apis;

import org.gitnex.tea4j.v2.CollectionFormats.*;
import org.gitnex.tea4j.v2.models.Compare;
import retrofit2.Call;
import retrofit2.http.*;

public interface GetApi {
  /**
   * Get commit comparison information
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param basehead compare two branches or commits (required)
   * @return Call&lt;Compare&gt;
   */
  @GET("repos/{owner}/{repo}/compare/{basehead}")
  Call<Compare> information(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("basehead") String basehead);
}
