package org.gitnex.tea4j.v2.apis;

import org.gitnex.tea4j.v2.CollectionFormats.*;
import org.gitnex.tea4j.v2.models.ActivityPub;
import retrofit2.Call;
import retrofit2.http.*;

public interface ActivitypubApi {
  /**
   * Returns the Person actor for a user
   *
   * @param userId user ID of the user (required)
   * @return Call&lt;ActivityPub&gt;
   */
  @GET("activitypub/user-id/{user-id}")
  Call<ActivityPub> activitypubPerson(@retrofit2.http.Path("user-id") Integer userId);

  /**
   * Send to the inbox
   *
   * @param userId user ID of the user (required)
   * @return Call&lt;Void&gt;
   */
  @POST("activitypub/user-id/{user-id}/inbox")
  Call<Void> activitypubPersonInbox(@retrofit2.http.Path("user-id") Integer userId);
}
