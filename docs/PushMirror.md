# PushMirror

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created** | [**Date**](Date.md) |  |  [optional]
**interval** | **String** |  |  [optional]
**lastError** | **String** |  |  [optional]
**lastUpdate** | [**Date**](Date.md) |  |  [optional]
**remoteAddress** | **String** |  |  [optional]
**remoteName** | **String** |  |  [optional]
**repoName** | **String** |  |  [optional]
**syncOnCommit** | **Boolean** |  |  [optional]
