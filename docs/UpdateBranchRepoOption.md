# UpdateBranchRepoOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | New branch name | 
