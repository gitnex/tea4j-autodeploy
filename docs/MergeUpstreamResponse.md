# MergeUpstreamResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mergeType** | **String** |  |  [optional]
