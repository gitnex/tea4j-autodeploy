# ActionTask

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdAt** | [**Date**](Date.md) |  |  [optional]
**displayTitle** | **String** |  |  [optional]
**event** | **String** |  |  [optional]
**headBranch** | **String** |  |  [optional]
**headSha** | **String** |  |  [optional]
**id** | **Long** |  |  [optional]
**name** | **String** |  |  [optional]
**runNumber** | **Long** |  |  [optional]
**runStartedAt** | [**Date**](Date.md) |  |  [optional]
**status** | **String** |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]
**url** | **String** |  |  [optional]
**workflowId** | **String** |  |  [optional]
