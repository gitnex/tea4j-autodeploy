# MergeUpstreamRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**branch** | **String** |  |  [optional]
