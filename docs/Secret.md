# Secret

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdAt** | [**Date**](Date.md) |  |  [optional]
**name** | **String** | the secret&#x27;s name |  [optional]
