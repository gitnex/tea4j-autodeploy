# Hook

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **Boolean** |  |  [optional]
**authorizationHeader** | **String** |  |  [optional]
**branchFilter** | **String** |  |  [optional]
**config** | **Map&lt;String, String&gt;** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**events** | **List&lt;String&gt;** |  |  [optional]
**id** | **Long** |  |  [optional]
**type** | **String** |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]
