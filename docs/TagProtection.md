# TagProtection

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdAt** | [**Date**](Date.md) |  |  [optional]
**id** | **Long** |  |  [optional]
**namePattern** | **String** |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]
**whitelistTeams** | **List&lt;String&gt;** |  |  [optional]
**whitelistUsernames** | **List&lt;String&gt;** |  |  [optional]
