# BadgeList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**body** | [**List&lt;Badge&gt;**](Badge.md) | in:body |  [optional]
